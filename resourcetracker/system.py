import numpy as np
import os
import pandas as pd
import psutil
import subprocess
import time

csv_header = ['timestamp', 'ppid', 'num_threads', 'memory_full_info_rss', 'memory_full_info_vms',
              'memory_full_info_shared', 'memory_full_info_text', 'memory_full_info_lib', 'memory_full_info_data',
              'memory_full_info_dirty', 'memory_full_info_uss', 'memory_full_info_pss', 'memory_full_info_swap',
              'nice', 'cpu_num', 'cpu_times_user', 'cpu_times_system', 'cpu_times_children_user',
              'cpu_times_children_system', 'cpu_times_iowait', 'cpu_percent', 'pid', 'status', 'num_fds',
              'memory_percent', 'create_time', 'ionice_ioclass', 'ionice_value', 'n_children',
              "read_chars", "write_chars", "read_count", "write_count", "read_bytes", "write_bytes",
              'gpu_name', 'gpu_uuid',
              'used_gpu_memory [MiB]', "gpu_memory_percent_used", "gpu_memory_percent_total", 'location', 'instance',
              'class_name', 'node_class', 'id', 'file_no', 'abs_timestamp'
              ]

class ResourceTracker:

    def __init__(self, pid = None):

        if pid is None:
            self.pid = os.getpid()
        else:
            self.pid = pid

    def get_psutils_stats(self,process):

        property_list = ["create_time", "cpu_num", "cpu_percent", "cpu_times", "ionice", "memory_full_info",
                         "memory_percent", "nice", "num_fds", "num_threads", "pid", "ppid", "status"]

        psutil_types = [psutil._pslinux.pfullmem, psutil._common.pctxsw, psutil._common.puids, psutil._common.pgids,
                        psutil._pslinux.pio, psutil._common.pionice, psutil._pslinux.pcputimes, psutil._pslinux.pmem,

                        ]

        process_dict = {"timestamp": time.time()}
        raw_dict = process.as_dict(property_list)
        raw_dict["cpu_percent"] = process.cpu_percent(0.01) / psutil.cpu_count()

        for k, v in raw_dict.items():
            if type(v) in psutil_types:
                process_dict.update({k + "_" + kk: vv for kk, vv in v._asdict().items()})
            elif isinstance(v, dict):
                process_dict.update({k + "_" + kk: vv for kk, vv in v.items()})
            elif isinstance(v, list):
                process_dict[k] = v
            else:
                process_dict[k] = v

        return process_dict

    def log_system_usage(self, cumulative=False):

        sum_columns = [
                          "cpu_percent", "cpu_times", "memory_percent", "num_fds", "num_threads"
                      ] + \
                      ["read_chars", "write_chars", "read_count", "write_count", "read_bytes", "write_bytes"]+\
                      [f"memory_full_info_{x}" for x in list(psutil.Process().memory_full_info()._asdict().keys())]+ \
                      [f"cpu_times_{x}" for x in list(psutil.Process().cpu_times()._asdict().keys())]

        try:
            process = psutil.Process(pid=self.pid)
        except psutil.NoSuchProcess as e:
            return None
        try:
            parent_stats = self.get_psutils_stats(process)
            process_dict = {}
            if cumulative and len(process.children(recursive=True)) > 0:
                process_df = pd.DataFrame([parent_stats] + [self.get_psutils_stats(p) for p in process.children(recursive=True)])
                for c in process_df.columns:
                    if c in sum_columns:
                        process_dict[c] = process_df[c].sum()
                    else:
                        if c in parent_stats.keys():
                            process_dict[c] = parent_stats[c]
            else:
                process_dict = parent_stats

            process_dict["n_children"] = len(process.children(recursive=True))

        except psutil.NoSuchProcess as e:
            return None

        return process_dict


    def get_disk_usage(self, process):
        try:
            disk_usage = process.io_counters()
            disk_dict = {
                "pid": os.getpid(),
                "timestamp": time.time(),
                "read_chars": disk_usage.read_chars,
                "write_chars": disk_usage.write_chars,
                "read_count": disk_usage.read_count,
                "write_count": disk_usage.write_count,
                "read_bytes": disk_usage.read_count,
                "write_bytes": disk_usage.write_count,
            }
        except (psutil.AccessDenied,psutil.NoSuchProcess) as e:
            return None

        return disk_dict


    def log_disk_usage(self, cumulative=False):
        pid = self.pid
        if cumulative:
            process_list = [psutil.Process(pid)] + psutil.Process(pid).children(recursive=True)
            disk_df = [self.get_disk_usage(p) for p in process_list]
            disk_df = pd.DataFrame([p for p in disk_df if p is not None])
            disk_dict = {}
            if len(disk_df) > 0:
                for c in ["read_chars", "write_chars", "read_count", "write_count", "read_bytes", "write_bytes"]:
                    disk_dict[c] = disk_df[c].sum()
                disk_dict["pid"] = pid
                disk_dict["timestamp"] = int(disk_df["timestamp"].iloc[0])
            else:
                return None
        else:
            disk_dict = self.get_disk_usage(psutil.Process(pid))

        return disk_dict

    @staticmethod
    def _get_gpu_usage():

        def get_data(o):
            data = []
            for i, d in enumerate([x.split(",") for x in o.split("\n")[1:]]):
                if d[0] == "": continue
                data.append(d)
            return data

        def get_header(o):
            header = o.split("\n")[0].split(",")
            header = [x.lstrip(" ").rstrip(" ") for x in header]
            return header

        def get_stats_df(o):

            df = pd.DataFrame.from_records(get_data(o), columns=get_header(o))
            df = df.rename({"uuid": "gpu_uuid"}, axis=1)

            df["memory.total [MiB]"] = df["memory.total [MiB]"].astype(int)
            df["memory.used [MiB]"] = df["memory.used [MiB]"].astype(int)

            return df

        def get_df(o):

            df = pd.DataFrame.from_records(get_data(o), columns=get_header(o))
            df["timestamp"] = pd.to_datetime(df["timestamp"])
            df["pid"] = df["pid"].astype(int)
            df["used_gpu_memory [MiB]"] = df["used_gpu_memory [MiB]"].astype(int)
            df["gpu_name"] = df["gpu_name"].astype(str)

            return df

        try:
            o = subprocess.check_output(
                ["nvidia-smi", "--query-compute-apps=timestamp,pid,gpu_name,gpu_uuid,used_memory",
                 "--format=csv,nounits"]).decode(
                "utf-8")
            s = subprocess.check_output(
                ["nvidia-smi", "--query-gpu=name,uuid,memory.total,memory.used", "--format=csv,nounits"]).decode(
                "utf-8")
        except FileNotFoundError:
            return pd.DataFrame(columns = ["pid","timestamp","gpu_name","gpu_uuid","used_gpu_memory [MiB]","gpu_memory_percent_used","gpu_memory_percent_total"])

        gpu_stats = get_stats_df(s)
        df = get_df(o)
        old_columns = df.columns.tolist()
        df = df.merge(gpu_stats[["gpu_uuid", "memory.used [MiB]", "memory.total [MiB]"]], on="gpu_uuid", how="left")

        df["timestamp"] = df["timestamp"].mean()
        df["gpu_memory_percent_used"] = np.round(df["used_gpu_memory [MiB]"] / df["memory.used [MiB]"] * 100,2)
        df["gpu_memory_percent_total"] = np.round(df["used_gpu_memory [MiB]"] / df["memory.total [MiB]"] * 100,2)

        return df[old_columns + ["gpu_memory_percent_used", "gpu_memory_percent_total"]]





    def log_gpu_usage(self, cumulative=False):
        pid = self.pid

        try:
            process = psutil.Process(pid=pid)
            df = self._get_gpu_usage()

            children = process.children(recursive=True)
        except psutil.NoSuchProcess:
            return None

        if cumulative:
            pids = [c.pid for c in children] + [pid]
        else:
            pids = [pid]
        df = df.loc[df.pid.isin(pids)]
        process_dict = {"timestamp": time.time(), "pid": pid, "gpu_name": "None",
                        "gpu_uuid": "None", "used_gpu_memory [MiB]": "0", "gpu_memory_percent_used": "0",
                        "gpu_memory_percent_total": "0"}
        if len(df) > 0:
            def agg_devices(df):
                def concat(r):
                    return ";".join(list(r.astype(str))).rstrip(",")
                ndf = pd.DataFrame(
                    [{
                        "timestamp":df["timestamp"].iloc[0],
                        "gpu_name":concat(df["gpu_name"]),
                        "gpu_uuid":concat(df["gpu_uuid"]),
                        "used_gpu_memory [MiB]":concat(df["used_gpu_memory [MiB]"]),
                        "gpu_memory_percent_used":concat(df["gpu_memory_percent_used"]),
                        "gpu_memory_percent_total":concat(df["gpu_memory_percent_total"]),
                    }]
                )

                return ndf
            df = df.groupby(["gpu_uuid","gpu_name","timestamp"],as_index = False).sum()
            row = df.groupby(["timestamp"],as_index=False).apply(agg_devices).iloc[0]

            process_dict["timestamp"] = row["timestamp"].timestamp()
            process_dict["pid"] = int(process.pid)
            process_dict["gpu_name"] = row["gpu_name"]
            process_dict["gpu_uuid"] = row["gpu_uuid"]
            process_dict["used_gpu_memory [MiB]"] = row["used_gpu_memory [MiB]"]
            process_dict["gpu_memory_percent_used"] = row["gpu_memory_percent_used"]
            process_dict["gpu_memory_percent_total"] = row["gpu_memory_percent_total"]


        return process_dict

    @staticmethod
    def get_cpu_usage(dt = 1, n_measurements = 1):

        interval = dt/n_measurements

        m_list = []
        for i in range(n_measurements):
            timestamp = time.time()
            m = [{"node": i, "usage": l, "timestamp": timestamp} for i, l in enumerate(psutil.cpu_percent(interval, percpu = True))]
            m_list += m

        return pd.DataFrame(m_list)
    @staticmethod
    def get_gpu_usage(dt = 1, n_measurements = 1):

        interval = dt / n_measurements
        m_list = []
        for i in range(n_measurements):
            s = time.time()
            m_list.append(ResourceTracker._get_gpu_usage())
            delta = interval - (time.time() - s)
            delta = 0 if delta < 0 else delta
            if n_measurements > 1:
                time.sleep(delta)

        return pd.concat(m_list)

if __name__ == '__main__':
    rt = ResourceTracker()

    s = time.time()
    # df = rt.get_gpu_usage()
    cpu_df = rt.get_cpu_usage(dt = 100, n_measurements = 100).groupby(["timestamp"]).mean().reset_index()

    import matplotlib.pyplot as plt
    import seaborn as sns
    sns.lineplot(x = "timestamp", y = "usage",data = cpu_df,palette = "tab20")
    # sns.lineplot(x = "timestamp", y = "gpu_memory_percent_total", hue = "gpu_uuid",data = df)
    plt.show()

